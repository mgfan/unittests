﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HrDepartment.Infrastructure.Implementation
{
    public class DbStorageStub : IStorage
    {
        public Task<T> AddAsync<T>(T item) where T : class
        {
            throw new NotImplementedException();
        }

        public Task DeleteAsync<T>(T item) where T : class
        {
            throw new NotImplementedException();
        }

        public Task<IList<T>> GetAllAsync<T>() where T : class
        {
            throw new NotImplementedException();
        }

        public Task<TItem> GetByIdAsync<TItem, TKey>(TKey id) where TItem : class
        {
            var testJobSeeker = new JobSeeker();
            testJobSeeker.BirthDate = new DateTime(DateTime.Now.Year - 20, 6, 6);
            testJobSeeker.Education = EducationLevel.University;
            testJobSeeker.Experience = 0;
            testJobSeeker.BadHabits = BadHabits.None;

            return Task.FromResult(true);
        }

        public Task<T> UpdateAsync<T>(T item) where T : class
        {
            throw new NotImplementedException();
        }
    }
}

using NUnit.Framework;
using HrDepartment.Application.Services;
using System;
using HrDepartment.Domain.Entities;
using HrDepartment.Domain.Enums;

namespace HrDepartament.Tests
{
    class JobSeekerRateServiceTests
    {
        [Test]
        public void Constructor_nullStancionService_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                JobSeekerRateService testJobSeekerRateService = new JobSeekerRateService(null);
            });
        }

        JobSeeker testJobSeeker;
        [OneTimeSetUp]
        public void JobSeekerSetup()
        {
            testJobSeeker = new JobSeeker();
        }

        [TestCase(16, EducationLevel.None, 0, BadHabits.None, ExpectedResult = 15)]
        [TestCase(20, EducationLevel.School, 2, BadHabits.Smoking, ExpectedResult = 30)]
        [TestCase(20, EducationLevel.College, 4, BadHabits.Alcoholism, ExpectedResult = 35)]
        [TestCase(20, EducationLevel.University, 8, BadHabits.Drugs, ExpectedResult = 15)]
        [TestCase(20, EducationLevel.University, 11, BadHabits.None, ExpectedResult = 95)]
        public int CalculateJobSeekerRatingAsync_JobSeekerInSanctionServiceFalse_returnedResult(int age, EducationLevel education, int experience, BadHabits habits)
        {
            testJobSeeker.BirthDate = new DateTime(DateTime.Now.Year - age, 6, 6);
            testJobSeeker.Education = education;
            testJobSeeker.Experience = experience;
            testJobSeeker.BadHabits = habits;
            SanctionServiceStubReturnFalse testSanctionService = new SanctionServiceStubReturnFalse();
            JobSeekerRateService testSeekerRateService = new JobSeekerRateService(testSanctionService);

            var res = testSeekerRateService.CalculateJobSeekerRatingAsync(testJobSeeker);

            return res.Result;
        }

        [Test]
        public void CalculateJobSeekerRatingAsync_JobSeekerInSanctionServiceTrue_0returnedResult()
        {
            testJobSeeker.BirthDate = new DateTime(DateTime.Now.Year - 20, 6, 6);
            testJobSeeker.Education = EducationLevel.University;
            testJobSeeker.Experience = 0;
            testJobSeeker.BadHabits = BadHabits.None;
            SanctionServiceStubReturnTrue testSanctionService = new SanctionServiceStubReturnTrue();
            JobSeekerRateService testSeekerRateService = new JobSeekerRateService(testSanctionService);

            var res = testSeekerRateService.CalculateJobSeekerRatingAsync(testJobSeeker);

            Assert.AreEqual(0, res.Result);
        }
    }
}
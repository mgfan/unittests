﻿using HrDepartment.Application.Services;
using System;
using HrDepartment.Domain.Entities;
using HrDepartment.Domain.Enums;
using NUnit.Framework;
using HrDepartment.Infrastructure.Implementation;
using Moq;
using System.Threading.Tasks;
using AutoMapper;
using HrDepartment.Infrastructure.Interfaces;
using HrDepartment.Application.Interfaces;

namespace HrDepartament.Tests
{
    class JobSeekerServiceTests
    {
        [Test]
        public void RateJobSeekerAsync_IdJobSeekerWithRaiting60_60returned()
        {
            JobSeeker testJobSeeker = new JobSeeker();
            var testDbStorage = new Mock<IStorage>();
            testDbStorage.Setup(item => item.GetByIdAsync<JobSeeker, int>(It.IsAny<int>())).Returns(Task.FromResult(testJobSeeker));
            var testJobSeekerRateService = new Mock<IRateService<JobSeeker>>();
            testJobSeekerRateService.Setup(item => item.CalculateJobSeekerRatingAsync(testJobSeeker)).Returns(Task.FromResult(60));
            var mapper = new Mock<IMapper>();
            EmailNotificationServiceStub testNotificationService = new EmailNotificationServiceStub();
            JobSeekerService testJobSeekerService = new JobSeekerService(testDbStorage.Object, testJobSeekerRateService.Object, mapper.Object, testNotificationService);

            var result = testJobSeekerService.RateJobSeekerAsync(1).Result;

            Assert.AreEqual(60, result);
        }

        [Test]
        public void RateJobSeekerAsync_IdJobSeekerWithRaiting100_NotifySendedAnd100returned()
        {
            JobSeeker testJobSeeker = new JobSeeker();
            var testDbStorage = new Mock<IStorage>();
            testDbStorage.Setup(item => item.GetByIdAsync<JobSeeker, int>(It.IsAny<int>())).Returns(Task.FromResult(testJobSeeker));
            var testJobSeekerRateService = new Mock<IRateService<JobSeeker>>();
            testJobSeekerRateService.Setup(item => item.CalculateJobSeekerRatingAsync(testJobSeeker)).Returns(Task.FromResult(100));
            var mapper = new Mock<IMapper>();
            var testNotificationService = new Mock<INotificationService>();
            JobSeekerService testJobSeekerService = new JobSeekerService(testDbStorage.Object, testJobSeekerRateService.Object, mapper.Object, testNotificationService.Object);

            var result = testJobSeekerService.RateJobSeekerAsync(1).Result;

            testNotificationService.Verify(send => send.NotifyRockStarFoundAsync(testJobSeeker));
            Assert.AreEqual(100, result);
        }
    }
}

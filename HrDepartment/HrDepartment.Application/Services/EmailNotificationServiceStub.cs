﻿using HrDepartment.Application.Interfaces;
using HrDepartment.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace HrDepartment.Application.Services
{
    public class EmailNotificationServiceStub : INotificationService
    {
        public Task NotifyRockStarFoundAsync(JobSeeker jobSeeker)
        {
           return Task.FromResult(true);
        }
    }
}

﻿using HrDepartment.Application.Interfaces;
using System;
using System.Threading.Tasks;

namespace HrDepartment.Application.Services
{
    public class SanctionServiceStubReturnFalse : ISanctionService
    {
        public Task<bool> IsInSanctionsListAsync(string lastName, string firstName, string middleName, DateTime birthDate)
        {
            return Task.FromResult(false);
        }
    }
}
